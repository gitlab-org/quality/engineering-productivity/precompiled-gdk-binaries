package main

import (
	"io"
	"log"
	"os"
	"precompiledbinaries"

	"github.com/xanzy/go-gitlab"
)

func main() {
	f, err := os.Open("workhorse-test")
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()
	err = upload("gitlab-workhorse", "workhorse_darwin-amd64", f)
	if err != nil {
		log.Fatalln(err)
	}
}

func upload(
	name string,
	filename string,
	file io.ReadCloser,
) error {
	token := os.Getenv("CI_JOB_TOKEN")
	client, err := gitlab.NewJobClient(token)
	if err != nil {
		return err
	}

	_, _, err = client.GenericPackages.PublishPackageFile(
		precompiledbinaries.PROJECT_ID,
		name,
		"main",
		filename,
		file,
		&gitlab.PublishPackageFileOptions{},
	)

	if err != nil {
		return err
	}

	return nil
}
