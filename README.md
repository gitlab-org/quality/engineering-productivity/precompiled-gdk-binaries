# Precompiled binaries for GDK

This is a test project for an Engineering Productivity initiative to
provide pre-compiled binaries for services that most developers won’t
modify, so their machines have to compile it themselves.
